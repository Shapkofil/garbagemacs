(defvar garbage/default-font-size 120)
(defvar garbage/default-variable-font-size 140)
(defvar garbage/org-config "~/.dotfiles/garbagemacs/Emacs.org")
(defvar garbage/org-roam-dir "~/org/roam")

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

  ;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(defalias 'yes-or-no-p 'y-or-n-p)

(defun delete-current-file ()
  "Delete the file associated with the current buffer."
  (interactive)
  (let ((filename (buffer-file-name)))
    (when filename
      (if (yes-or-no-p (format "Are you sure you want to delete %s?" filename))
          (progn
            (delete-file filename)
            (kill-buffer)
            (message "File '%s' deleted and buffer killed." filename))
        (message "Deletion of file '%s' canceled." filename)))))

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(global-set-key (kbd "S-C-h") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-l") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-k") 'shrink-window)
(global-set-key (kbd "S-C-j") 'enlarge-window)

;; Reloads the init file
(defun reload-init-file()
  (interactive)
  (load user-init-file)
  (org-mode-restart))

;; Open Emacs.org
(defun open-emacs-config()
  (interactive)
  (find-file garbage/org-config))

;; Leader key setup
(use-package general
  :config
  (general-create-definer garbage/leader-keys
    :keymaps '(normal visual emacs)
    :prefix "SPC"
    :global-prefix "SPC")

  (garbage/leader-keys
    "e"   '(:ignore t :which-key "Emacs")
    "er"  '(reload-init-file :which-key "Reload init.el")
    "ec"  '(open-emacs-config :which-key "Open Emacs.org")
    "m"   '(man :which-key "Manual Pages")
    "c"   '(compile :which-key "compile")))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

  ;; Undo tree allows redo
(use-package undo-tree
  :after evil
  :config
  (global-undo-tree-mode))
(evil-set-undo-system 'undo-tree)

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package winner-mode
  :ensure nil
  :bind (:map evil-window-map
         ("u" . winner-undo)
         ("U" . winner-redo))
  :config
  (winner-mode))

;; Multiple cursors in evil mode
(use-package evil-mc
  :init
  (global-evil-mc-mode 1)
  :bind (
	 ("C->" . evil-mc-make-and-goto-next-match)
	 ("C-<" . evil-mc-make-and-goto-prev-match)
	 ))

;; Handling backups
(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

;; Handling backups
(setq undo-tree-directory-alist `(("." . ,(expand-file-name "tmp/undo-tree/" user-emacs-directory))))



;; auto-save-mode doesn't create the path automatically!
(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

(setq warning-minimum-level :error)

(defvar find-file-root-prefix (if (featurep 'xemacs) "/[sudo/root@localhost]" "/sudo:root@localhost:" )
  "*The filename prefix used to open a file with `find-file-root'.")

(defvar find-file-root-history nil
  "History list for files found using `find-file-root'.")

(defvar find-file-root-hook nil
  "Normal hook for functions to run after finding a \"root\" file.")

(defun find-file-root ()
  "*Open a file as the root user.
   Prepends `find-file-root-prefix' to the selected file name so that it
   maybe accessed via the corresponding tramp method."

  (interactive)
  (require 'tramp)
  (let* ( ;; We bind the variable `file-name-history' locally so we can
	 ;; use a separate history list for "root" files.
	 (file-name-history find-file-root-history)
	 (name (or buffer-file-name default-directory))
	 (tramp (and (tramp-tramp-file-p name)
		     (tramp-dissect-file-name name)))
	 path dir file)

    ;; If called from a "root" file, we need to fix up the path.
    (when tramp
      (setq path (tramp-file-name-localname tramp)
	    dir (file-name-directory path)))

    (when (setq file (read-file-name "Find file (UID = 0): " dir path))
      (find-file (concat find-file-root-prefix file))
      ;; If this all succeeded save our new history list.
      (setq find-file-root-history file-name-history)
      ;; allow some user customization
      (run-hooks 'find-file-root-hook))))

(global-set-key [(control x) (control r)] 'find-file-root)

(pdf-tools-install)

(defun garbage/visual-tweaks ()
  "Visual Tweeks functions"
  (interactive)
  ;; pixel size fix
  (setq frame-resize-pixelwise t)
  (setq inhibit-startup-message t)
  (scroll-bar-mode -1)        ; Disable visible scrollbar
  (tool-bar-mode -1)          ; Disable the toolbar
  (tooltip-mode -1)           ; Disable tooltips
  (menu-bar-mode -1)            ; Disable the menu bar
  ;; Set up the visible bell
  (setq visible-bell t))

;; In case of standalone emacs
(garbage/visual-tweaks)

(defun garbage/font-faces ()
"Setting up font faces"

(defun set-conditional-font (face primary-font fallback-font size)
  "Set PRIMARY-FONT if available, otherwise FALLBACK-FONT with SIZE."
  (if (find-font (font-spec :name primary-font))
      (set-face-attribute face nil :font primary-font :height size)
    (set-face-attribute face nil :font fallback-font :height size)))

(set-conditional-font
 'default
 "Comic Code Ligatures Medium" "Fira Code"
 garbage/default-font-size)

(set-conditional-font
 'fixed-pitch
 "Comic Code Ligatures Medium" "Fira Code"
 garbage/default-font-size)

(set-conditional-font
 'variable-pitch
 "VG5000" "DejaVu Sans"
 garbage/default-variable-font-size))


(garbage/font-faces)

(setq split-height-threshold 80
      split-width-threshold 120)

(defun garbage/split-window-sensibly (&optional window)
  (let ((window (or window (selected-window))))
    (or 
	(and (window-splittable-p window t)
	     ;; Split window horizontally.
	     (with-selected-window window
	       (split-window-right)))
	(and (window-splittable-p window)
	     ;; Split window vertically.
	     (with-selected-window window
	       (split-window-below)))
	(and
         ;; If WINDOW is the only usable window on its frame (it is
         ;; the only one or, not being the only one, all the other
         ;; ones are dedicated) and is not the minibuffer window, try
         ;; to split it vertically disregarding the value of
         ;; `split-height-threshold'.
         (let ((frame (window-frame window)))
           (or
            (eq window (frame-root-window frame))
            (catch 'done
              (walk-window-tree (lambda (w)
                                  (unless (or (eq w window)
                                              (window-dedicated-p w))
                                    (throw 'done nil)))
                                frame nil 'nomini)
              t)))
	 (not (window-minibuffer-p window))
	 (let ((split-height-threshold 0))
	   (when (window-splittable-p window)
	     (with-selected-window window
	       (split-window-below))))))))

(setq split-window-preferred-function #'garbage/split-window-sensibly)

(column-number-mode)
(global-display-line-numbers-mode t)

;; Set relative line mode to default
(setq display-line-numbers t)
(setq display-line-numbers-type 'relative)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                org-agenda-mode-hook
                compilation-mode-hook
                term-mode-hook
                vterm-mode-hook
                helpful-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(set-frame-parameter (selected-frame) 'alpha '(95 . 90))
(add-to-list 'default-frame-alist '(alpha . (95 . 90)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(defun garbage/theme-load ()
    "Frame time modus theme config"
     (setq modus-themes-italic-constructs t
	modus-themes-bold-constructs t
	modus-themes-region '(bg-only no-extend)
	modus-themes-links '(no-color bold)
	modus-themes-org-blocks 'gray-background 
	modus-themes-common-palette-overrides 'modus-themes-preset-overrides-faint
	modus-themes-common-palette-overrides '((fg-line-number-inactive "gray50")
						(fg-line-number-active fg-main)
						(bg-line-number-inactive unspecified)
						(bg-line-number-active unspecified))
	modus-themes-headings '((1 . (monochrome bold 1.2))
				(2 . (monochrome semibold 1.16))
				(3 . (monochrome semibold 1.12))
				(t . (semibold 1.1))))
     (load-theme 'modus-vivendi 1)
     (set-face-attribute 'line-number-current-line nil :weight 'normal)
     (set-face-attribute 'line-number-major-tick nil :weight 'normal)
     (set-face-attribute 'line-number-minor-tick nil :weight 'normal))

(garbage/theme-load)

  (use-package simple-modeline
    :functions simple-modeline--format
    :config
    (defun simple-modeline--format (left-segments right-segments)
      "Return a string of `window-width' length containing LEFT-SEGMENTS and RIGHT-SEGMENTS, aligned respectively."
      (let* ((left (simple-modeline--format-segments left-segments))
	     (right (simple-modeline--format-segments right-segments))
	     (reserve (length right)))
	(concat
	 left
	 (propertize " "
		     'display `((space :align-to (- right (- 0 right-margin) ,reserve)))
		     'face '(:inherit simple-modeline-space))
	 right)))
    :hook (after-init . simple-modeline-mode)) 

  ;; Customize modeline elements
  (custom-set-variables
   '(simple-modeline-segments
     '((simple-modeline-segment-modified
       simple-modeline-segment-buffer-name
       simple-modeline-segment-position)
      (simple-modeline-segment-input-method
       simple-modeline-segment-eol
       simple-modeline-segment-encoding
       simple-modeline-segment-vc
       simple-modeline-segment-misc-info
       simple-modeline-segment-process
       simple-modeline-segment-major-mode)) ""))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

(use-package no-littering
  :custom
  (setq no-littering-etc-directory
	(expand-file-name "config/" user-emacs-directory))
  (setq no-littering-var-directory
	(expand-file-name "data/" user-emacs-directory)))

(recentf-mode 1)
(setq recentf-max-menu-items 500)
(setq recentf-max-saved-items 500)
(run-at-time nil (* 5 60) 'recentf-save-list)

(defun recentf-open-files-compl ()
  (interactive)
;;  (setq recentf-list (sort recentf-list (lambda (a b)
;;					  (time-less-p (nth 5 (file-attributes b))
;;						       (nth 5 (file-attributes a))))))
  (let* ((tocpl (mapcar (lambda (x) (cons (file-name-nondirectory x) x))
                        recentf-list))
         (fname (completing-read "File name: " tocpl nil nil)))
    (when fname
      (find-file (cdr (assoc-string fname tocpl))))
    )) 


(global-set-key "\C-x\ \C-r" 'recentf-open-files-compl)

(use-package vertico
  :ensure t
  :bind (:map vertico-map
         ("C-j" . vertico-next)
         ("C-k" . vertico-previous)
         ("C-f" . vertico-exit)
         :map minibuffer-local-map
         ("M-h" . backward-kill-word))
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package savehist
  :init
  (savehist-mode))

(use-package marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  (marginalia-align 'right)
  :init
  (marginalia-mode))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(defun get-password (password-name &optional tag)
  "Retrieves password from pass dir."
  (with-temp-buffer
    (let ((status (apply 'call-process "pass" nil t nil (list "show" password-name))))
      (when (= status 0)
        (goto-char (point-min))
        (cond ((null tag)
               (buffer-substring-no-properties (point-min) (line-end-position)))
              (t
               (re-search-forward (concat "^" tag ":\\s-*\\(.*\\)$") nil t)
               (match-string 1)))))))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1)
  :diminish yas-global-mode)

(use-package yasnippet-snippets
  :after yasnippet
  :ensure t)

(require 'warnings)
(add-to-list 'warning-suppress-types '(yasnippet backquote-change))

(use-package company
  :ensure t)

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "->") t nil)))))

(defun do-yas-expand ()
  (let ((yas/fallback-behavior 'return-nil))
    (yas/expand)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))

(dolist (mode '(c-mode-hook c++-mode objc-mode
                python-mode-hook
                glsl-mode-hook
                R-mode-hook
                lisp-mode-hook
                emacs-lisp-mode
                sh-mode-hook))
  (add-hook mode (lambda () (company-mode 1))))

(global-set-key (kbd "TAB") 'tab-indent-or-complete)

(use-package helpful
  :bind (("C-h f" . #'helpful-callable)
         ("C-h v" . #'helpful-variable)
         ("C-h k" . #'helpful-key)
         ("C-h s" . #'helpful-symbol)
         ("C-c C-d" . #'helpful-at-point)))

(add-to-list 'warning-suppress-types 'backquote-change)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package fixmee
  :ensure t
  :init
  ;; disable fixmee default global keybindings.
  (setq fixmee-smartrep-prefix nil
        fixmee-view-listing-keystrokes nil
        fixmee-goto-nextmost-urgent-keystrokes nil
        fixmee-goto-next-by-position-keystrokes nil
        fixmee-goto-prevmost-urgent-keystrokes nil
        fixmee-goto-previous-by-position-keystrokes nil)
  (require 'button-lock)

  :config
  (setq fixmee-cache-refresh-interval 30)

  (set-face-attribute 'fixmee-notice-face nil
                      :background "dark orange" :foreground "#222222"
                      :weight 'bold
                      )

  ;; (add-to-list 'fixmee-exclude-modes 'xxx-mode)
  (global-fixmee-mode 1))

(use-package pyvenv
  :ensure t)

(defvar pyvenv-conda-home "/home/kiko/.conda/envs")
(defvar pyvenv-poetry-home "/home/kiko/.cache/pypoetry/virtualenvs")

(defun pyvenv-virtualenv-list-conda (dir &optional noerror)
  (let ((workon-home dir)
        (result nil))
    (if (not (file-directory-p dir))
        (when (not noerror)
          (error "Can't find a workon home directory, set $WORKON_HOME"))
      (dolist (name (directory-files workon-home))
        (when (or (file-exists-p (format "%s/%s/bin/activate"
                                         workon-home name))
                  (file-exists-p (format "%s/%s/bin/python"
                                         workon-home name))
                  (file-exists-p (format "%s/%s/Scripts/activate.bat"
                                         workon-home name))
                  (file-exists-p (format "%s/%s/python.exe"
                                         workon-home name)))
          (setq result (cons name result))))
      (sort result (lambda (a b)
                     (string-lessp (downcase a)
                                   (downcase b)))))))

(defun pyvenv-workon-conda (name)
  (interactive
   (list
    (completing-read "Work on: " (pyvenv-virtualenv-list-conda pyvenv-conda-home)
                     nil t nil 'pyvenv-workon-history nil nil)))

	(unless (member name (list "" nil pyvenv-virtual-env-name))
	  (pyvenv-activate (format "%s/%s"
				   pyvenv-conda-home
				   name))))


(defun pyvenv-workon-poetry (name)
  (interactive
   (list
    (completing-read "Work on: " (pyvenv-virtualenv-list-conda pyvenv-poetry-home)
                     nil t nil 'pyvenv-workon-history nil nil)))

	(unless (member name (list "" nil pyvenv-virtual-env-name))
	  (pyvenv-activate (format "%s/%s"
				   pyvenv-poetry-home
				   name))))

(use-package ein
  :custom
  (ein:output-area-inlined-images t "Make inline image render inside EIN mode")
  (org-startup-with-latex-preview t "Render Latex elements in EIN buffers"))

(garbage/leader-keys
     "p"  '(:ignore t :which-key "Python")
     "pw" '(pyvenv-workon :which-key "Workon")
     "pc" '(pyvenv-workon-conda :which-key "Workon Conda")
     "pp" '(pyvenv-workon-poetry :which-key "Workon Poetry")
     "pj" '(ein:run :which-key "Run Jupyter")
     "pk" '(ein:stop :which-key "Kill Jupyter"))

(setq-default c-default-style "bsd"
	            c-basic-offset 8
              tab-width 8
              indent-tabs-mode t)

(use-package rustic
  :init
  (setq rustic-analyzer-command '("~/.cargo/bin/rust-analyzer")
	rustic-analyzer-command '("rustup" "run" "stable" "rust-analyzer")
  rustic-lsp-client 'eglot))

(use-package julia-mode)

(use-package glsl-mode)

;;(load-file "/home/kiko/code/emacs/glsl-viewer/glsl-viewer-mode.el")
;;(add-hook 'glsl-mode-hook (glsl-viewer-mode 1))

(use-package wgsl-mode)

(use-package ess)

(use-package ess-site
  :ensure ess
  :commands R
  :hook (ess-mode-hook . subword-mode))

      (use-package auctex
	  :defer t
	  :ensure t
	  :config
	  (setq TeX-auto-save t)
	  (setq TeX-auto-save t)
	  (setq TeX-parse-self t)
	  (setq-default TeX-master nil)
	  (setq TeX-PDF-mode t)
      )

      (defun config-zathura ()
	"Configuration for zathura view in Latex"
	(add-to-list 'TeX-view-program-list
		       '("Zathura"
			 ("zathura %o"
			  (mode-io-correlate
			   " --synctex-forward %n:0:\"%b\" -x \"emacsclient --socket-name=%sn +%{line} %{input}\""))
			 "zathura"))
	(setcar (cdr (assoc 'output-pdf TeX-view-program-selection)) "Zathura"))

      (add-hook 'LaTeX-mode-hook 'config-zathura)

(use-package nix-mode
  :mode "\\.nix\\'")

(use-package haskell-mode)

(use-package yaml-mode)

(use-package pdf-tools)
(pdf-loader-install)

(use-package tidal
  :init 
  (setq tidal-boot-script-path "~/.config/tidal/BootTidal.hs"))

;; (add-to-list 'load-path "~/.local/src/supercollider/editors/scel/el")
;; (require 'sclang)
;; 
;; (use-package sclang-extensions)
;; (use-package sclang-snippets)

(when (boundp 'tramp-connection-properties) 
  (add-to-list 'tramp-connection-properties
	       (list (regexp-quote "/sshx:user@host:")
		     "remote-shell" "/usr/bin/zsh")))

(customize-set-variable 'tramp-encoding-shell "/usr/bin/zsh")

(use-package magit
  :init
  (setq magit-auto-revert-mode nil)
  :bind
  (("C-c m s" . magit-status)
   ("C-c m l" . magit-log))
  :custom
  (setq magit-show-long-lines-warning nil))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs '(c++-mode . ("clangd")))
  (add-to-list 'eglot-server-programs '(c-mode . ("clangd")))
  (add-to-list 'eglot-server-programs '(swift-mode . ("clangd")))
  ;; (add-to-list 'eglot-server-programs '(python-mode . ("pyright-langserver" "--stdio")))
  (add-to-list 'eglot-server-programs '(python-mode . ("pylsp")))
  (add-to-list 'eglot-server-programs '(haskell-mode . ("haskell-language-server-wrapper" "--lsp")))
  (add-to-list 'eglot-server-programs '(rust-mode . ("rust-analyzer")))
  (add-to-list 'eglot-server-programs '(latex-mode . ("texlab")))
  (add-to-list 'eglot-server-programs '(nix-mode . ("nil")))
  (add-to-list 'eglot-server-programs '(wgsl-mode . ("wgsl_analyzer"))))

(dolist (mod '(
	       c-mode-hook
	       haskell-mode-hook
	       python-mode-hook
	       rust-mode))
  (add-hook mod 'eglot-ensure))

(add-hook 'eglot--managed-mode-hook 'company-mode)

(use-package direnv
 :config
 (direnv-mode))

(use-package nix-sandbox)
(use-package nixos-options)

(use-package wc-mode)

(use-package dictionary)

(defun my-dictionary-search ()
  (interactive)
  (let ((word (current-word))
        (enable-recursive-minibuffers t)
        (val))
    (setq val (read-from-minibuffer
               (concat "Word"
                       (when word
                         (concat " (" word ")"))
                       ": ")))
    (dictionary-new-search
     (cons (cond
             ((and (equal val "") word)
              word)
             ((> (length val) 0)
              val)
             (t
              (error "No word to lookup")))
           dictionary-default-dictionary))))

(garbage/leader-keys
      "w"  '(:ignore t :which-key "Writting")
      "ws"  '(ispell-word :which-key "Check word")
      "wS"  '(ispell :which-key "Check Spelling")
      "wf"  '(flyspell-mode :which-key "Toggle Flyspell-mode")
      "wd"  '(my-dictionary-search :which-key "Dictionary"))

(defconst garbage/bib-libraries '("~/bib-lib/lib.bib"))
(defconst garbage/main-pdfs-library-paths '("~/bib-lib/pdfs/"))

(use-package elfeed
  :config
  (setq elfeed-feeds
	'("https://blog.jessfraz.com"
	  "http://arxiv.org/rss/math.GR"
	  "http://arxiv.org/rss/math.GN"
	  "http://arxiv.org/rss/math.QA"
	  "http://arxiv.org/rss/math.ST"
	  "http://arxiv.org/rss/cs"
	  "https://www.mfa.bg/bg/rss"
	  )))

(use-package citar
  :config
  (setq citar-bibliography garbage/bib-libraries
	citar-library-paths garbage/main-pdfs-library-paths
    citar-file-extensions '("pdf" "org" "md")
    citar-file-open-functions '(("html" . citar-file-open-external)
			       (t . find-file-other-window))))

(setq org-directory "~/org/")

(defun garbage/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(defun garbage/org-mode-setup ()
  (org-indent-mode 1)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(use-package org
  :hook (org-mode . garbage/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")

  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-hide-emphasis-markers t)

  ;; inline images
  (setq org-startup-with-inline-images t)
  (setq org-image-actual-width (list 450))

  ;; 1 lines that end the nightmare of org src editing
  (setq org-src-preserve-indentation t)

  (setq org-archive-location "~/org/Archive.org::datetree/* Finished Tasks")

  (setq org-agenda-files 
        (list
          (concat org-directory "Inbox.org")
          (concat org-directory "Misc.org")
          (concat org-directory "Schedule-Gcal.org")))

  (setq org-todo-keywords
    '((sequence "NEXT(n)" "TODO(t)" "|" "DONE(d!)")))

  (setq org-clock-sound "~/.dotfiles/.emacs.d/sounds/whiterose.wav")

  ;; Save Org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-tag-alist
    '((:startgroup)
       ; Put mutually exclusive tags here
       (:endgroup)
       ("@home" . ?h)
       ("@work" . ?w)
       ("@school" . ?s)
       ("shopping" . ?S)
       ("art" . ?a)
       ("high" . ?H)
       ("idea" . ?i)))

  (add-hook 'org-capture-mode-hook #'org-align-all-tags)
  (garbage/org-font-setup))

  (defun garbage/org-mode-visual-fill ()
    (setq visual-fill-column-width 100
    visual-fill-column-center-text t)
    (visual-fill-column-mode 1))

  (use-package visual-fill-column
    :hook (org-mode . garbage/org-mode-visual-fill))

  (defun garbage/visual-fill-column-split-window-sensibly (&optional window)
    (let ((margins (window-margins window))
	  new)
      ;; unset the margins and try to split the window
      (when (buffer-local-value 'visual-fill-column-mode (window-buffer window))
	(set-window-margins window nil))
      (unwind-protect
	  (setq new (garbage/split-window-sensibly window))
	(when (not new)
	  (set-window-margins window (car margins) (cdr margins))))))

  (setq split-window-preferred-function #'garbage/visual-fill-column-split-window-sensibly)

;; This is needed as of Org 9.2
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("hs" . "src haskell"))

(use-package org-autolist)

(add-hook 'org-mode-hook (lambda () (org-autolist-mode)))

(use-package org-pomodoro
    :config
    (defun org-pomodoro-sound (type)
      (concat user-emacs-directory "sounds/game_bell.wav")))

(add-hook 'org-pomodoro-finished-hook #'org-save-all-org-buffers)

(add-hook 'auto-save-hook 'org-save-all-org-buffers)

;; Create a link to a non existant node without a promp
(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))

(defun garbage/courses-only (node)
    (interactive)
    (let ((tags (org-roam-node-tags node)))
        (member "Course" tags)))

(defun org-roam-node-insert-course (arg &rest args)
  (interactive "P")
  (org-roam-node-insert 'garbage/courses-only))

 
(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-database-connector 'sqlite-builtin)
  (org-roam-directory garbage/org-roam-dir)
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "\n%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n#+STARTUP: latexpreview\n")
      :unnarrowed t)
     ("c" "course topic" plain
      (file "~/org/roam/templates/coursetopic.org")
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n#+STARTUP: latexpreview\n")
      :unnarrowed t)
     ))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert-immediate)
         ("C-c n I" . org-roam-node-insert)
         ("C-c n c" . org-roam-node-insert-course)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :config
  (setq org-roam-node-display-template
	(concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode))

(use-package websocket
    :after org-roam)

(use-package org-roam-ui
    :after org-roam 
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
    (shell . t)
    (python . t)))

(push '("conf-unix" . conf-unix) org-src-lang-modes)

;;Annoying tabulation fix
(setq org-src-tab-acts-natively t)

;; Automatically tangle our Emacs.org config file when we save it
(defun garbage/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
                      (expand-file-name garbage/org-config))
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'garbage/org-babel-tangle-config)))

(setq org-cite-global-bibliography garbage/bib-libraries
      org-cite-insert-processor 'citar
      org-cite-follow-processor 'citar
      org-cite-activate-processor 'citar
      org-cite-export-processors '((latex biblatex)
                                   (t csl)))

(setq org-format-latex-options
      (plist-put org-format-latex-options :scale 1.5))

;; Handles Latex snippets inside org mode
(defun garbage/org-latex-yas ()
  "Activate org and LaTeX yas expansion in org-mode buffers."
  (yas-minor-mode)
  (yas-activate-extra-mode 'latex-mode))

(add-hook 'org-mode-hook #'garbage/org-latex-yas)

(use-package org-fragtog
  :hook (org-mode . org-fragtog-mode))

(garbage/leader-keys
    "o"  '(:ignore t :which-key "Org Mode")
    "oc" '(org-capture :which-key "Org Capture")
    "oa" '(org-agenda :which-key "Org Agenda")
    "ot" '(:ignore t :which-key "Org Timer")
    "ott" '(org-timer-set-timer :which-key "Set Timer")
    "otp" '(org-timer-pause-and-continue :which-key "Pause")
    "ots" '(org-timer-stop :which-key "Stop")
    "op"  '(org-pomodoro :which-key "pomodoro"))

(eval-after-load 'org-agenda
  '(progn
     (evil-set-initial-state 'org-agenda-mode 'normal)
     (evil-define-key 'normal org-agenda-mode-map
       (kbd "<RET>") 'org-agenda-switch-to
       (kbd "\t") 'org-agenda-goto

       "q" 'org-agenda-quit
       "r" 'org-agenda-redo
       "S" 'org-save-all-org-buffers
       "gj" 'org-agenda-goto-date
       "gJ" 'org-agenda-clock-goto
       "gm" 'org-agenda-bulk-mark
       "go" 'org-agenda-open-link
       "s" 'org-agenda-schedule
       "+" 'org-agenda-priority-up
       "," 'org-agenda-priority
       "-" 'org-agenda-priority-down
       "y" 'org-agenda-todo-yesterday
       "n" 'org-agenda-add-note
       "t" 'org-agenda-todo
       ":" 'org-agenda-set-tags
       ";" 'org-timer-set-timer
       "i" 'org-agenda-clock-in
       "o" 'org-agenda-clock-out
       "u" 'org-agenda-bulk-unmark
       "x" 'org-agenda-exit
       "j"  'org-agenda-next-line
       "k"  'org-agenda-previous-line
       "vt" 'org-agenda-toggle-time-grid
       "va" 'org-agenda-archives-mode
       "vw" 'org-agenda-week-view
       "vl" 'org-agenda-log-mode
       "vd" 'org-agenda-day-view
       "vc" 'org-agenda-show-clocking-issues
       "g/" 'org-agenda-filter-by-tag
       "O" 'delete-other-windows
       "gh" 'org-agenda-holiday
       "gv" 'org-agenda-view-mode-dispatch
       "f" 'org-agenda-refile
       "c" 'helm-org-capture-templates
       "e" 'org-agenda-set-effort
       "n" nil  ; evil-search-next
       "{" 'org-agenda-manipulate-query-add-re
       "}" 'org-agenda-manipulate-query-subtract-re
       "A" 'org-agenda-toggle-archive-tag
       "." 'org-agenda-goto-today
       "0" 'evil-digit-argument-or-evil-beginning-of-line
       "<" 'org-agenda-filter-by-category
       ">" 'org-agenda-date-prompt
       "F" 'org-agenda-follow-mode
       "D" 'org-agenda-deadline
       "H" 'org-agenda-holidays
       "J" 'org-agenda-next-date-line
       "K" 'org-agenda-previous-date-line
       "L" 'org-agenda-recenter
       "P" 'org-agenda-show-priority
       "R" 'org-agenda-clockreport-mode
       "Z" 'org-agenda-sunrise-sunset
       "T" 'org-agenda-show-tags
       "X" 'org-agenda-clock-cancel
       "[" 'org-agenda-manipulate-query-add
       "g\\" 'org-agenda-filter-by-tag-refine
       "]" 'org-agenda-manipulate-query-subtract)))

(defun garbage/frame-tweaks (frame)
  (garbage/visual-tweaks)
  (garbage/theme-load)
  (garbage/font-faces)
  (garbage/org-font-setup))

(if (daemonp)
        (add-hook 'after-make-frame-functions #'garbage/frame-tweaks))
